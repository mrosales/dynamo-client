package deploy

import (
	"os"
	"strconv"
)

func ECSDown(composeFile string) *os.ProcessState {
	return RunShellCommand("/usr/local/bin/ecs-cli", []string{"ecs-cli", "compose", "-f", composeFile, "down"})
}

func ECSScale(composeFile string, scale int) *os.ProcessState {
	return RunShellCommand("/usr/local/bin/ecs-cli", []string{"ecs-cli", "compose", "-p", "dynamo", "-f", composeFile, "scale", strconv.Itoa(scale)})
}
