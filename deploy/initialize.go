package deploy

import (
	"bitbucket.org/mrosales/dynamo/rpc"
	"fmt"
	"strconv"
)

func InitializeRoot(root rpc.RegisterInstanceArgs, n int, r int, w int) (*rpc.InitializeInstanceArgs, error) {
	addr := "http://" + root.PublicIP + ":" + root.RPC_PORT
	client := rpc.NewRPCClient(addr)

	rpcPort, _ := strconv.ParseInt(root.RPC_PORT, 0, 64)

	arg := rpc.InitializeInstanceArgs{
		IsRoot:            true,
		RootAddr:          "localhost",
		RootRPCPort:       rpcPort,
		IsInitialized:     true,
		NumReplicas:       int64(n),
		ReadParticipants:  int64(r),
		WriteParticipants: int64(w),
	}
	response, err := client.InitializeInstanceConfig(&arg)

	if err != nil {
		fmt.Printf("Err Registering: %s\n", addr)
		fmt.Println(err)
		return nil, err
	}

	fmt.Printf("Configuring %s as root -> ", root.PublicIP)
	if response.Success {
		fmt.Println("success!")
	} else {
		fmt.Println("failure!")
	}
	return &arg, err
}

func InitializeSlaves(slaves []rpc.RegisterInstanceArgs, root rpc.RegisterInstanceArgs, n int, r int, w int) error {

	rpcPort, _ := strconv.ParseInt(root.RPC_PORT, 0, 64)

	slaveArg := rpc.InitializeInstanceArgs{
		IsRoot:            false,
		RootAddr:          root.LocalIP,
		RootRPCPort:       rpcPort,
		IsInitialized:     true,
		NumReplicas:       int64(n),
		ReadParticipants:  int64(r),
		WriteParticipants: int64(w),
	}

	for _, slave := range slaves {
		addr := "http://" + slave.PublicIP + ":" + slave.RPC_PORT
		client := rpc.NewRPCClient(addr)

		response, err := client.InitializeInstanceConfig(&slaveArg)

		if err != nil {
			fmt.Printf("Err Initializing Slave: %s\n", addr)
			fmt.Println(err)
			return err
		}

		fmt.Printf("Configuring %s as slave. -> ", slave.PublicIP)
		if response.Success {
			fmt.Println("success!")
		} else {
			fmt.Println("failure!")
		}
	}
	return nil
}

func RegisterInstances(instanceArgs []rpc.RegisterInstanceArgs) error {
	for _, arg := range instanceArgs {
		addr := "http://" + arg.PublicIP + ":" + arg.RPC_PORT
		client := rpc.NewRPCClient(addr)

		response, err := client.RegisterInstanceConfig(&arg)

		if err != nil {
			fmt.Printf("Err Registering: %s\n", addr)
			fmt.Println(err)
			return err
		}

		fmt.Printf("Configuring %s -> ", addr)
		if response.Success {
			fmt.Println("success!")
		} else {
			fmt.Println("failure!")
		}
	}

	fmt.Printf("\nView Configuration at:\n")

	for _, arg := range instanceArgs {
		fmt.Printf("http://%s:%s/config\n", arg.PublicIP, arg.HTTP_PORT)
	}
	return nil
}
