package deploy

import (
	"bitbucket.org/mrosales/dynamo/rpc"
	"fmt"
	"log"
	"strings"
)

// copied from mrosales/dyanamo main.go
const (
	CONN_HOST = ""
	CONN_PORT = 8000
	CONN_TYPE = "tcp"
	RPC_PORT  = 9000
)

func GetConfigInfo() ([]rpc.RegisterInstanceArgs, *InstanceConfigData) {
	cfg := NewInstanceConfigData()
	args := GetRPCInstanceArgs(cfg)
	return args, cfg
}

func Instances() {
	instanceArgs, _ := GetConfigInfo()

	fmt.Printf("Public IP\tEC2 Instance\tPrivate IP\tRPC\tHTTP\n")
	for _, arg := range instanceArgs {
		fmt.Printf("%s\t%s\t%s\t:%s\t:%s\n", arg.PublicIP, arg.EC2InstanceId, arg.LocalIP, arg.RPC_PORT, arg.HTTP_PORT)
	}
}

func Configure(n int, r int, w int) {
	fmt.Printf("Configuring nodes for params: n=%d, r=%d, w=%d\n", n, r, w)

	instanceArgs, _ := GetConfigInfo()

	err := RegisterInstances(instanceArgs)
	if err != nil {
		log.Println(err)
		log.Fatal("Error registering instances. Exiting...")
	}

	root, slaves := instanceArgs[0], instanceArgs[1:]

	InitializeRoot(root, n, r, w)
	InitializeSlaves(slaves, root, n, r, w)
}

func StartLogs(host string) {
	ip := strings.Split(host, ":")[0]
	if ip == "" {
		instanceArgs, _ := GetConfigInfo()
		ip = instanceArgs[0].PublicIP
	}
	fmt.Printf(">> Connecting to remote logs\n")
	state := RunShellCommand("/usr/bin/ssh", []string{"/usr/bin/ssh", "ec2-user@" + ip, "docker ps -q | head -n1 | xargs docker logs -f"})
	fmt.Printf("<< Exited remote logs: %s\n", state.String())
}

func StartSSH(host string) {
	ip := strings.Split(host, ":")[0]
	if ip == "" {
		instanceArgs, _ := GetConfigInfo()
		ip = instanceArgs[0].PublicIP
	}

	fmt.Printf(">> Starting remote ssh shell\n")
	state := RunShellCommand("/usr/bin/ssh", []string{"/usr/bin/ssh", "ec2-user@" + ip})
	fmt.Printf("<< Exited remote session: %s\n", state.String())
}

// Generates server list for localhost & local docker instance
func LocalTestArgs() []rpc.RegisterInstanceArgs {
	docker := rpc.RegisterInstanceArgs{
		LocalIP:                "docker.local",
		PublicIP:               "docker.local",
		RPC_PORT:               "9000",
		HTTP_PORT:              "80",
		ECSContainerInstanceId: "na",
		EC2InstanceId:          "na",
	}

	local := rpc.RegisterInstanceArgs{
		LocalIP:                "192.168.1.38",
		PublicIP:               "192.168.1.38",
		RPC_PORT:               "9000",
		HTTP_PORT:              "8000",
		ECSContainerInstanceId: "na",
		EC2InstanceId:          "na",
	}

	args := make([]rpc.RegisterInstanceArgs, 2)
	args[0] = docker
	args[1] = local
	return args
}
