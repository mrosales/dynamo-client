package deploy

import (
	"os"
)

func RunShellCommand(name string, cmd []string) *os.ProcessState {
	pa := os.ProcAttr{
		Files: []*os.File{os.Stdin, os.Stdout, os.Stderr},
		Dir:   ".",
	}

	// Start up a new shell.
	proc, err := os.StartProcess(name, cmd, &pa)
	if err != nil {
		panic(err)
	}

	// Wait until user exits the shell
	state, err := proc.Wait()
	if err != nil {
		panic(err)
	}

	// Keep on keepin' on.
	return state
}
