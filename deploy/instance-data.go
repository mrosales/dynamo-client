package deploy

import (
	"bitbucket.org/mrosales/dynamo/rpc"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ecs"
	"log"
	"strconv"
)

type InstanceConfigData struct {
	Session         *session.Session
	EC2Instances    map[string]*ec2.Instance
	ECSInstances    []*ecs.ContainerInstance
	ECSPortBindings map[string][]*ecs.NetworkBinding
	ECSContainers   map[string]*ecs.Container
}

func NewInstanceConfigData() *InstanceConfigData {
	sess := session.New(&aws.Config{Region: aws.String("us-east-1")})
	icd := &InstanceConfigData{Session: sess}
	icd.populateECSInstances()
	icd.populateEC2Instances()
	icd.populateECSPortBindings()
	return icd
}

func GetRPCInstanceArgs(cfg *InstanceConfigData) []rpc.RegisterInstanceArgs {
	instanceArgs := make([]rpc.RegisterInstanceArgs, 0, len(cfg.ECSInstances))

	for _, val := range cfg.ECSInstances {
		instId := *val.Ec2InstanceId
		inst := cfg.EC2Instances[instId]

		bindings := cfg.ECSPortBindings[*val.ContainerInstanceArn]

		httpPort := int64(CONN_PORT)
		rpcPort := int64(RPC_PORT)
		for _, val := range bindings {
			p := *val.ContainerPort
			if p == CONN_PORT {
				httpPort = *val.HostPort
			} else if p == RPC_PORT {
				rpcPort = *val.HostPort
			}
		}

		arg := rpc.RegisterInstanceArgs{
			LocalIP:                *inst.PrivateIpAddress,
			PublicIP:               *inst.PublicIpAddress,
			RPC_PORT:               strconv.FormatInt(rpcPort, 10),
			HTTP_PORT:              strconv.FormatInt(httpPort, 10),
			ECSContainerInstanceId: *val.ContainerInstanceArn,
			EC2InstanceId:          instId,
		}
		instanceArgs = append(instanceArgs, arg)
	}
	return instanceArgs
}

func (ins *InstanceConfigData) populateEC2Instances() {
	svc := ec2.New(ins.Session, nil)

	// Call the DescribeInstances Operation
	resp, err := svc.DescribeInstances(nil)
	if err != nil {
		panic(err)
	}

	// resp has all of the response data, pull out instance IDs:
	instances := make(map[string]*ec2.Instance)
	for idx, _ := range resp.Reservations {
		for _, inst := range resp.Reservations[idx].Instances {
			instances[*inst.InstanceId] = inst
		}
	}
	ins.EC2Instances = instances
}

func (ins *InstanceConfigData) populateECSInstances() {
	ecsSVC := ecs.New(ins.Session, nil)

	listParams := &ecs.ListContainerInstancesInput{
		Cluster: aws.String("cloud"),
	}
	resp, err := ecsSVC.ListContainerInstances(listParams)

	if err != nil {
		fmt.Println("Error listing ecs instances: ", err)
		log.Fatal("Exiting...")
	}

	if len(resp.ContainerInstanceArns) == 0 {
		ins.ECSInstances = make([]*ecs.ContainerInstance, 0)
		return
	}

	describeParams := &ecs.DescribeContainerInstancesInput{
		ContainerInstances: resp.ContainerInstanceArns,
		Cluster:            aws.String("cloud"),
	}

	resp2, err := ecsSVC.DescribeContainerInstances(describeParams)

	if err != nil {
		fmt.Println("Error describing ecs instances: ", err)
		log.Fatal("Exiting...")
	}

	ins.ECSInstances = resp2.ContainerInstances
}

func (ins *InstanceConfigData) populateECSPortBindings() {
	ecsSVC := ecs.New(ins.Session, nil)

	params := &ecs.ListTasksInput{
		Cluster: aws.String("cloud"),
	}

	output, _ := ecsSVC.ListTasks(params)

	describeTasksInput := &ecs.DescribeTasksInput{
		Tasks:   output.TaskArns,
		Cluster: aws.String("cloud"),
	}

	response, _ := ecsSVC.DescribeTasks(describeTasksInput)

	bindings := make(map[string][]*ecs.NetworkBinding)
	containers := make(map[string]*ecs.Container)

	for _, task := range response.Tasks {
		container := task.Containers[0]
		containers[*task.ContainerInstanceArn] = container
		bindings[*task.ContainerInstanceArn] = container.NetworkBindings
	}

	ins.ECSContainers = containers
	ins.ECSPortBindings = bindings
}
