# Dynamo Client & Load Balancer

Made by Micah Rosales (mhr35)  
21 December 2015

This repository holds the code for the Load Balancer and test suite for the [Dynamo-style distributed key-value store.](http://bitbucket.org/mrosales/dynamo)

Load balancing is performed using the following heuristics:

For a distributed key-value store, a value is not held on just any key. Using this particular system, a known consistent-hashing algorithm is used to allocate the key-space amongst the server list. Using this knowledge, we can speed up a request by moving the knowledge of this distribution to the load-balancer (which is the client in this case).

The default implementation of a load-balancer simply chooses a pseudorandom node to direct the request to. In many cases, the node selected cannot actually fullfill the request by itself. Allowing the balancing application to use additional system information guarantees that the balancer will not send a request to a node that then needs to forward it to another node. This reduces latency by cutting out extra inter-node communication to fulfill a request.

Secondly, in order to choose an appropriate node, we maintain a moving average of the response times for all of the nodes in the server list. Each time a request is made, the moving average is updated. Then, when a new request is issued, the balancer queries the preference list for the given key (as described above) and then selects the node that has the lowest average latency.

This second heuristic allows the load balancer to stop sending load to a server that is slowing down its response rate, which causes a better distribution of load.

## Code Overview

It will not be possible to run this code on your local system because it requires a fully set up go environment, but more importantly, it requires a working deployment of the [dynamo clone](http://bitbucket.org/mrosales/dynamo), which was implemented for CPSC 458.

The entry point of the system is in [`main.go`](main.go). It establishes a fancy CLI interface with lots of different options for viewing the status of, manipulating, and interacting with the distributed system. The [`deploy`](deploy) folder contains the code responsible for configuring the system to work properly. The [`generate-graphs.r`](generate-graphs.r) file is an `R` script that generates the plots that appear below.

The most important section for this project however, is in the [`balancer`](balancer) package. [`balancer.go`](balancer/balancer.go) implements the logic described above for making the decision and uses the utility type `MovingAverage` in [`movingaverage.go`](balancer/movingaverage.go)


## Performance Boost

Below are some plots of latency over time in the balanced and unbalanced systems.

As the plots show, the latency with the balancer enabled had far fewer spikes of poor performance and lead to lower and more stable latencies.

The images are in the [plots folder](https://bitbucket.org/mrosales/dynamo-client/src/d5e712ce2c2ea59aa7458ef2a48b039bf13b420d/plots/?at=master) if they do not render on this page.


<img src="plots/unbalanced/write-latency-w1.png?raw=true" width="400px" height="auto" />

<img src="plots/balanced/write-latency-w1.png?raw=true" width="400px" height="auto" />

<img src="plots/unbalanced/read-latency-r2.png?raw=true" width="400px" height="auto" />

<img src="plots/balanced/read-latency-r2.png?raw=true" width="400px" height="auto" />
