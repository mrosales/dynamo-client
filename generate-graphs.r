
library(plyr)

# returns the latency quantile for specified quantile
qLatency <- function(x, quant) {
  return(arrange(x[x$latency < quantile(x$latency, quant),], start))
}

read.benchmarkData <- function(filename="tests/put-test-r2w2.csv") {
  x <- read.csv(filename, as.is=T)
  names(x) <- c("start", "key", "latency")
  x$latency <- as.numeric(x$latency, na.rm=T)
  x$start <- as.numeric(x$start)
  x <- x[!is.na(x$latency),][!is.na(x$start),]
  x$startMS <- x$start / 1000
  return(x)
}

setPNGOutput <- function(filename, subdir="plots") {
  if (!dir.exists(subdir)) {
    dir.create(subdir)
  }
  path <- file.path(subdir, filename)
  png(filename=path, width=600, height=400, units="px")
}

########### WRITE LATENCY W=2 ##################

xw <- read.benchmarkData("testdata/balanced/put-test-r2w2.csv")
x95 <- qLatency(xw, .95)
summary(x95$latency)["Median"]

setPNGOutput(filename="write-latency-w2.png")
plot(x95$latency ~ x95$startMS,
     type="l",
     main="Balanced 95% Write Latency Over Time (N=3, W=2)",
     ylim=c(150, 400),
     xlim=c(0, 105),
     xlab="Time (s)",
     ylab="Latency (ms)")

dev.off()

########### WRITE LATENCY W=1 ##################

xw <- read.benchmarkData("testdata/balanced/put-test-r1w1.csv")
x95 <- qLatency(xw, .95)
summary(x95$latency)["Median"]

setPNGOutput(filename="write-latency-w1.png")
plot(x95$latency ~ x95$startMS,
     type="l",
     main="Balanced 95% Write Latency Over Time (N=3, W=1)",
     ylim=c(150, 400),
     xlim=c(0, 105),
     xlab="Time (s)",
     ylab="Latency (ms)")

dev.off()

########### READ LATENCY R=1 ##################
xr <- read.benchmarkData("testdata/balanced/get-test-r1w1.csv")
xr95 <- qLatency(xr, 0.95)
summary(xr95$latency)["Median"]

setPNGOutput(filename="read-latency-r1.png")
plot(xr95$latency ~ xr95$startMS,
     type="l",
     main="Balanced 95% Read Latency Over Time (N=3, R=1)",
     ylim=c(150, 900),
     xlim=c(0, 105),
     xlab="Time (s)",
     ylab="Latency (ms)")
dev.off()

########### READ LATENCY R=2 ##################

xr <- read.benchmarkData("testdata/balanced/get-test-r2w2.csv")
xr95 <- qLatency(xr, 0.95)
summary(xr95$latency)["Median"]

setPNGOutput(filename="read-latency-r2.png")
plot(xr95$latency ~ xr95$startMS,
     type="l",
     main="Balanced 95% Read Latency Over Time (N=3, R=2)",
     ylim=c(150, 900),
     xlim=c(0, 105),
     xlab="Time (s)",
     ylab="Latency (ms)")

dev.off()

################################################

