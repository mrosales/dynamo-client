package balancer

import (
	"bitbucket.org/mrosales/dynamo-client/deploy"
	"bitbucket.org/mrosales/dynamo/rpc"
	"errors"
	"fmt"
	"github.com/serialx/hashring"
	"math/rand"
	"sort"
	"time"
)

const (
	MA_SIZE = 10 // Moving-average window
)

type Balancer struct {
	HashRing *hashring.HashRing

	MovingAverages map[string]*MovingAverage
	NumReplicas    int
}

func NewBalancer() *Balancer {
	b := &Balancer{}
	b.UpdateHashRing()
	return b
}

func (b *Balancer) UpdateHashRing() {
	instanceArgs, _ := deploy.GetConfigInfo()
	host := instanceArgs[0].PublicIP + ":" + instanceArgs[0].RPC_PORT

	client := rpc.NewRPCClient("http://" + host)
	resp, err := client.ListServers()
	if err != nil {
		fmt.Println(err)
		return
	}

	// since the instances use the internal IP addresses,
	// we need to manually convert them to the external ones
	instanceMap := make(map[string]string)
	for _, arg := range instanceArgs {
		localAddr := arg.LocalIP + ":" + arg.RPC_PORT
		remoteAddr := arg.PublicIP + ":" + arg.RPC_PORT
		instanceMap[localAddr] = remoteAddr
	}

	// make sure the servers are sorted by ascending local IP address
	servers := resp.ServerList
	b.NumReplicas = int(resp.NodeConfig.NumReplicas)
	sort.Strings(servers)

	publicServers := make([]string, 0, len(servers))
	movingAverages := make(map[string]*MovingAverage)
	for _, s := range servers {
		publicServers = append(publicServers, instanceMap[s])
		movingAverages[instanceMap[s]] = NewMovingAverage(MA_SIZE)
	}

	if len(publicServers) != len(servers) {
		panic("Could not map all servers to public ips")
	}

	b.HashRing = hashring.New(publicServers)
	b.MovingAverages = movingAverages
}

func (b *Balancer) GetPreferenceList(key []byte, size int) (nodes []string, ok bool) {

	if b.HashRing == nil {
		return nil, false
	}

	return b.HashRing.GetNodes(string(key), size)
}

func (b *Balancer) lowestMovingAverage(serverList []string) (n string) {
	defer func() {
		if r := recover(); r != nil {
			// fmt.Println("recovery: ", r)
			n = serverList[0]
		}
	}()
	if len(serverList) <= 0 {
		return ""
	}
	ma := b.MovingAverages
	node := serverList[0]
	// fmt.Printf("servers: %v \n", serverList)
	minNode, min := node, ma[node].Value()
	for _, node := range serverList {
		ma := ma[node]
		val := ma.Value()
		if val < min {
			minNode, min = node, val
		}
	}
	// fmt.Printf("Using %s (%fms)\n", minNode, min)
	return minNode
}

func (b *Balancer) Put(key []byte, val []byte) (*rpc.PutReply, error) {
	pl, ok := b.GetPreferenceList(key, b.NumReplicas)

	if !ok {
		return nil, errors.New("Error fetching preference list")
	}

	node := b.lowestMovingAverage(pl)

	if node == "" {
		return nil, errors.New("Could not pull a node from the preference list.")
	}

	// fmt.Printf("%s <- %s\n", node, string(key))
	client := rpc.NewRPCClient("http://" + node)

	start := time.Now()
	reply, err := client.Put(key, val)
	durationMS := time.Since(start).Nanoseconds() / 1000 / 1000

	b.MovingAverages[node].Add(float64(durationMS))
	return reply, err
}

func (b *Balancer) Get(key []byte) (*rpc.GetReply, error) {
	pl, ok := b.GetPreferenceList(key, b.NumReplicas)

	if !ok {
		return nil, errors.New("Error fetching preference list")
	}

	node := b.lowestMovingAverage(pl)

	if node == "" {
		return nil, errors.New("Could not pull a node from the preference list.")
	}

	// fmt.Printf("%s <- %s\n", node, string(key))
	client := rpc.NewRPCClient("http://" + node)

	start := time.Now()
	reply, err := client.Get(key)
	durationMS := time.Since(start).Nanoseconds() / 1000 / 1000

	b.MovingAverages[node].Add(float64(durationMS))

	return reply, err
}
