package balancer

type MovingAverage struct {
	size    int
	bins    []float64
	average float64
	i       int
}

func NewMovingAverage(size int) *MovingAverage {
	return &MovingAverage{
		size:    size,
		bins:    make([]float64, size),
		average: 0,
		i:       0,
	}
}

func (m *MovingAverage) Add(val float64) {
	m.average += (val - m.bins[m.i]) / float64(m.size)
	m.bins[m.i] = val
	m.i = (m.i + 1) % m.size
}
func (m *MovingAverage) Value() float64 {
	return m.average
}
