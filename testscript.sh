#!/bin/sh

CLIENT=dynamo-client
export DOCKER_COMPOSE_FILE=../dynamo/docker-compose.yml

teardown() {
    $CLIENT ecs down
}

n=3
r=2
w=1

setup() {
    n=$1
    r=$2
    w=$3
    $CLIENT ecs scale 8 && \
    $CLIENT configure -n "$n" -r "$r" -w "$w"
}

ITERATIONS=10000
OUT_DIR="tests"

[[ -d "$OUT_DIR" ]] || mkdir "$OUT_DIR"

test() {
    $CLIENT test "$1" --iterations "$ITERATIONS" --outfile "$OUT_DIR/$1-test-r${r}w${w}.csv" --balance
}

teardown
setup 3 2 2
test "put"
test "get"

teardown
setup 3 1 1
test "put"
test "get"