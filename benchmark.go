package main

import (
	"bufio"
	"fmt"
	"github.com/dchest/uniuri"
	"os"
	"strconv"
	"sync"
	"time"
)

type WriteBenchmark struct {
	StartTime  int64
	Key        string
	DurationMS int64
}

type ReadBenchmark WriteBenchmark

func benchmarkWrite(iterations int, outfile string) {
	f, _ := os.OpenFile(outfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	testStart := time.Now()
	defer f.Close()
	w := bufio.NewWriter(f)

	out := make(chan WriteBenchmark, iterations/4)
	done := make(chan bool)
	go func() {
		completed := 0
		lastUpdate := time.Now()
		for result := range out {
			completed += 1
			w.WriteString(strconv.FormatInt(result.StartTime, 10) + ",")
			w.WriteString(result.Key + ",")
			w.WriteString(strconv.FormatInt(result.DurationMS, 10) + "\n")
			if time.Since(lastUpdate).Seconds() > 5.0 {
				fmt.Printf("Completed %.2f%%\n", float64(completed)/float64(iterations))
				lastUpdate = time.Now()
			}
		}
		done <- true
	}()

	var wg sync.WaitGroup

	for i := 0; i < iterations; i++ {
		wg.Add(1)
		go func() {
			// time since the test started
			// used for plotting latency over time
			timeDelta := time.Since(testStart).Nanoseconds() / 1000 / 1000

			start := time.Now()
			k, v := uniuri.New(), uniuri.New()
			cmd := &Command{CmdTypePut, []byte(k), []byte(v)}
			ExecCommand(cmd, true)
			elapsed := time.Since(start).Nanoseconds() / 1000 / 1000

			out <- WriteBenchmark{timeDelta, k, elapsed}
			wg.Done()
		}()

		// delay 10ms between requests
		<-time.After(time.Millisecond * 10)
	}

	wg.Wait()
	close(out)
	<-done

	w.Flush()
}

func benchmarkRead(iterations int, outfile string) {
	f, _ := os.OpenFile(outfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	testStart := time.Now()
	defer f.Close()
	w := bufio.NewWriter(f)

	out := make(chan ReadBenchmark, iterations/4)
	done := make(chan bool)
	go func() {
		completed := 0
		lastUpdate := time.Now()
		for result := range out {
			completed += 1
			w.WriteString(strconv.FormatInt(result.StartTime, 10) + ",")
			w.WriteString(result.Key + ",")
			w.WriteString(strconv.FormatInt(result.DurationMS, 10) + "\n")
			if time.Since(lastUpdate).Seconds() > 5.0 {
				fmt.Printf("Completed %.2f%%\n", float64(completed)/float64(iterations))
				lastUpdate = time.Now()
			}
		}
		done <- true
	}()

	var wg sync.WaitGroup

	for i := 0; i < iterations; i++ {
		wg.Add(1)
		go func() {
			// time since the test started
			// used for plotting latency over time
			timeDelta := time.Since(testStart).Nanoseconds() / 1000 / 1000

			start := time.Now()
			k := uniuri.New()
			cmd := &Command{CmdTypeGet, []byte(k), nil}
			ExecCommand(cmd, true)
			elapsed := time.Since(start).Nanoseconds() / 1000 / 1000

			out <- ReadBenchmark{timeDelta, k, elapsed}
			wg.Done()
		}()

		// delay 10ms between requests
		<-time.After(time.Millisecond * 10)
	}

	wg.Wait()
	close(out)
	<-done

	w.Flush()
}
