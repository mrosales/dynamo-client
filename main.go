package main

import (
	"bitbucket.org/mrosales/dynamo-client/balancer"
	"bitbucket.org/mrosales/dynamo-client/deploy"
	"bitbucket.org/mrosales/dynamo/rpc"
	"bufio"
	"flag"
	"fmt"
	"github.com/codegangsta/cli"
	"github.com/dchest/uniuri"
	"io"
	"os"
	"strconv"
	"strings"
)

type CmdType string

const (
	CmdTypeHelp = "CmdTypeHelp"
	CmdTypeGet  = "CmdTypeGet"
	CmdTypePut  = "CmdTypePut"
)

type Command struct {
	Type  CmdType
	Key   []byte
	Value []byte
}

type KVClient interface {
	Put(key []byte, val []byte) (*rpc.PutReply, error)
	Get(key []byte) (*rpc.GetReply, error)
}

var Config struct {
	Client KVClient
}

var host = flag.String("host", "localhost:9000", "Host to coordinate requests")
var outfile = flag.String("outfile", "temp/outfile.csv", "File to hold output for the test")
var iterationsFlag = flag.Int("iterations", 100, "Number of iterations for automated test")

func main() {
	app := cli.NewApp()
	app.Name = "client"
	app.Usage = "Client for the dynamo distributed key-value store"

	hostFlag := cli.StringFlag{
		Name:   "host",
		Value:  "",
		Usage:  "host to connect to",
		EnvVar: "DYNAMO_HOST",
	}

	balanceFlag := cli.BoolFlag{
		Name:  "balance",
		Usage: "Enable load balancer",
	}
	app.Flags = []cli.Flag{
		hostFlag,
	}

	composeFileFlag := cli.StringFlag{
		Name:   "compose-file",
		Value:  "docker-compose.yml",
		Usage:  "File to use with docker-compose",
		EnvVar: "DOCKER_COMPOSE_FILE",
	}

	testFlags := []cli.Flag{
		cli.IntFlag{
			Name:  "iterations",
			Value: 1000,
			Usage: "number of iterations",
		},
		cli.StringFlag{
			Name:  "outfile",
			Value: "temp/test-output.csv",
			Usage: "File to output test results to",
		},
		hostFlag,
		balanceFlag,
	}

	app.Commands = []cli.Command{
		{
			Name:    "shell",
			Aliases: []string{"s"},
			Usage:   "open an interactive shell to the host",
			Action: func(c *cli.Context) {
				host := c.String("host")
				Config.Client = rpc.NewRPCClient("http://" + host)
				fmt.Println("Opening interactive shell to:", host)
				interactive()
			},
			Flags: []cli.Flag{
				hostFlag,
			},
		},
		{
			Name:    "configure",
			Aliases: []string{"cfg"},
			Usage:   "runs the configure script",
			Action: func(c *cli.Context) {
				deploy.Configure(c.Int("num-replicas"), c.Int("read-participants"), c.Int("write-participants"))
			},
			Flags: []cli.Flag{
				hostFlag,
				cli.IntFlag{
					Name:  "num-replicas, n",
					Value: 3,
					Usage: "Number of replicas made by the host",
				},
				cli.IntFlag{
					Name:  "read-participants, r",
					Value: 2,
					Usage: "Number of required read participants",
				},
				cli.IntFlag{
					Name:  "write-participants, w",
					Value: 2,
					Usage: "Number of required write participants",
				},
			},
		},
		{
			Name:    "logs",
			Aliases: []string{"l"},
			Usage:   "Connects to the logs of root instance",
			Action: func(c *cli.Context) {
				deploy.StartLogs(c.String("host"))
			},
			Flags: []cli.Flag{
				hostFlag,
			},
		},
		{
			Name:  "ssh",
			Usage: "Opens an SSH connection to the specified instance",
			Action: func(c *cli.Context) {
				deploy.StartSSH(c.String("host"))
			},
			Flags: []cli.Flag{
				hostFlag,
			},
		},
		{
			Name:    "instances",
			Aliases: []string{"i"},
			Usage:   "lists the various instances",
			Action: func(c *cli.Context) {
				deploy.Instances()
			},
		},
		{
			Name:    "test",
			Aliases: []string{"t"},
			Usage:   "run a test mode",
			Subcommands: []cli.Command{
				{
					Name:    "put",
					Aliases: []string{"write", "p"},
					Usage:   "test the put fuctionality",
					Action: func(c *cli.Context) {
						if c.Bool("balance") {
							fmt.Printf("Using load balancer to handle requests\n")
							Config.Client = balancer.NewBalancer()
						} else {
							Config.Client = rpc.NewRPCClient("http://" + c.String("host"))
						}
						iterations := c.Int("iterations")
						outfile := c.String("outfile")
						fmt.Printf("Starting %d put() requests, logging to %s\n", iterations, outfile)
						benchmarkWrite(iterations, outfile)
					},
					Flags: testFlags,
				},
				{
					Name:    "get",
					Aliases: []string{"read", "g"},
					Usage:   "test the get fuctionality",
					Action: func(c *cli.Context) {
						if c.Bool("balance") {
							fmt.Printf("Using load balancer to handle requests\n")
							Config.Client = balancer.NewBalancer()
						} else {
							Config.Client = rpc.NewRPCClient("http://" + c.String("host"))
						}
						iterations := c.Int("iterations")
						outfile := c.String("outfile")
						fmt.Printf("Starting %d get() requests, logging to %s\n", iterations, outfile)
						benchmarkRead(iterations, outfile)
					},
					Flags: testFlags,
				},
			},
		},
		{
			Name:  "ecs",
			Usage: "run a wrapped ecs-cli command",
			Subcommands: []cli.Command{
				{
					Name:  "down",
					Usage: "Stop all running ECS containers",
					Action: func(c *cli.Context) {
						composeFile := c.String("compose-file")
						deploy.ECSDown(composeFile)
					},
					Flags: []cli.Flag{
						composeFileFlag,
					},
				},
				{
					Name:  "scale",
					Usage: "Scale the ecs instance",
					Action: func(c *cli.Context) {
						composeFile := c.String("compose-file")
						scale, _ := strconv.ParseInt(c.Args().First(), 0, 64)
						deploy.ECSScale(composeFile, int(scale))
					},
					Flags: []cli.Flag{
						composeFileFlag,
					},
				},
			},
		},
	}

	app.Run(os.Args)
}

func interactive() {
	exit := false

	var command *Command = nil
	for !exit {
		command = ReadCmd()
		if command != nil {
			ExecCommand(command, false)
		} else {
			exit = true
		}
	}
}

func ReadCmd() *Command {
	exit := false
	hasValidCmd := false

	reader := bufio.NewReader(os.Stdin)
	for !hasValidCmd && !exit {
		fmt.Print("❯ ")
		line, err := reader.ReadString('\n')

		if err == io.EOF {
			exit = true
			break
		} else if err != nil {
			fmt.Printf("Error: %+v", err)
			break
		}

		argv := strings.SplitN(strings.TrimSpace(line), " ", 3)
		argc := len(argv)
		argv[0] = strings.ToLower(argv[0])
		if argc > 2 {
			if argv[0] == "put" || argv[0] == "p" {
				return &Command{CmdTypePut, []byte(argv[1]), []byte(argv[2])}
			}
		} else if argc > 1 {
			if argv[0] == "get" || argv[0] == "g" {
				return &Command{CmdTypeGet, []byte(argv[1]), nil}
			}
		} else if argc > 0 {
			if argv[0] == "put-random" || argv[0] == "pr" {
				k, v := uniuri.New(), uniuri.New()
				return &Command{CmdTypePut, []byte(k), []byte(v)}
			}
		}
		return &Command{CmdTypeHelp, nil, nil}
	}
	return nil
}

func ExecCommand(cmd *Command, quiet bool) {
	if cmd.Type == CmdTypeHelp {
		fmt.Printf("get|put|help [key] [value]\n")
	} else if cmd.Type == CmdTypePut {
		putResult, err := Config.Client.Put(cmd.Key, cmd.Value)

		if !quiet {
			if err != nil {
				fmt.Printf("put error: %+v\n", err)
			} else {
				fmt.Printf("[StashService.Put] Success=%v\n", putResult.Success)
			}
		}

	} else if cmd.Type == CmdTypeGet {
		getResult, err := Config.Client.Get(cmd.Key)
		if !quiet {
			if err != nil {
				fmt.Printf("get error: %+v\n", err)
			} else {
				fmt.Printf("[StashService.Get] = '%s'='%s'\n", string(getResult.Key), string(getResult.Value))
			}
		}
	}
}
